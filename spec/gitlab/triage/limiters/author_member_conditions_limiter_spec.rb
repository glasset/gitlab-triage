require 'spec_helper'

require 'gitlab/triage/limiters/author_member_conditions_limiter'

describe Gitlab::Triage::Limiters::AuthorMemberConditionsLimiter do
  let(:type) { :author }

  it_behaves_like 'a member limiter'
end
