require_relative 'base_conditions_limiter'
require_relative '../url_builders/url_builder'

module Gitlab
  module Triage
    module Limiters
      class MemberConditionsLimiter < BaseConditionsLimiter
        SOURCES = %w[project group].freeze
        CONDITIONS = %w[member_of not_member_of].freeze

        def initialize(resource, condition, net = {})
          @net = net
          @network = net[:network]
          super(resource, condition)
        end

        def self.limiter_parameters
          [
            {
              name: :source,
              type: String,
              values: SOURCES
            },
            {
              name: :condition,
              type: String,
              values: CONDITIONS
            },
            {
              name: :source_id,
              type: [Numeric, String]
            }
          ]
        end

        def initialize_variables(condition)
          @source = condition[:source].to_sym
          @condition = condition[:condition].to_sym
          @source_id = condition[:source_id]
        end

        def resource_value
          @resource[member_field][:username] if @resource[member_field]
        end

        def condition_value
          members.map do |member|
            member[:username]
          end
        end

        def calculate
          return false unless resource_value

          case @condition
          when :member_of
            condition_value.include?(resource_value)
          when :not_member_of
            !condition_value.include?(resource_value)
          end
        end

        def members
          @members ||= @network.query_api(@net[:token], member_url)
        end

        def member_url
          UrlBuilders::UrlBuilder.new(net_opts).build
        end

        private

        def net_opts
          {
            host_url: @net[:host_url],
            api_version: @net[:api_version],
            resource_type: 'members',
            source: @source == :group ? 'groups' : 'projects',
            source_id: @source_id,
            params: { per_page: 100 }
          }
        end
      end
    end
  end
end
