require_relative 'member_conditions_limiter'

module Gitlab
  module Triage
    module Limiters
      class AssigneeMemberConditionsLimiter < MemberConditionsLimiter
        def member_field
          :assignee
        end
      end
    end
  end
end
