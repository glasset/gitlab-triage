require_relative 'member_conditions_limiter'

module Gitlab
  module Triage
    module Limiters
      class AuthorMemberConditionsLimiter < MemberConditionsLimiter
        def member_field
          :author
        end
      end
    end
  end
end
