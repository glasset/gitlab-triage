require 'spec_helper'

require 'gitlab/triage/limiters/assignee_member_conditions_limiter'

describe Gitlab::Triage::Limiters::AssigneeMemberConditionsLimiter do
  let(:type) { :assignee }

  it_behaves_like 'a member limiter'
end
