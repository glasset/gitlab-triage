RSpec.shared_examples 'a member limiter', :type do
  let(:username) { 'joe_bloggs' }
  let(:resource) do
    {
      type => {
        username: username
      }
    }
  end
  let(:group_id) { 9970 }
  let(:condition) do
    {
      source: 'group',
      condition: 'member_of',
      source_id: group_id
    }
  end

  let(:usernames) { [username, 'b'] }
  let(:members) do
    [
      { username: usernames[0] },
      { username: usernames[1] }
    ]
  end

  let(:net) do
    {}
  end

  before do
    allow(subject).to receive(:members).and_return(members)
  end

  subject { described_class.new(resource, condition, net) }

  it_behaves_like 'a limiter'

  context '#resource_value' do
    it 'has the correct value for updated_at attribute' do
      expect(subject.resource_value).to eq(username)
    end
  end

  context '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(usernames)
    end
  end

  context '#calculate' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to eq(true)
    end

    context 'resource member is nil' do
      before do
        resource[type] = nil
      end

      it 'calculate false' do
        expect(subject.calculate).to eq(false)
      end
    end

    context do
      let(:condition) do
        {
          source: 'group',
          condition: 'not_member_of',
          source_id: 9970
        }
      end

      it 'calculate false given wrong condition' do
        expect(subject.calculate).to eq(false)
      end

      context 'resource member is nil' do
        before do
          resource[type] = nil
        end

        it 'calculate false' do
          expect(subject.calculate).to eq(false)
        end
      end
    end
  end

  context '#member_url' do
    let(:net) do
      {
        host_url: 'https://gitlab.com',
        api_version: 'v4'
      }
    end

    it 'generates the correct url' do
      expect(subject.member_url).to eq("https://gitlab.com/api/v4/groups/#{group_id}/members?per_page=100")
    end

    context 'for projects' do
      let(:project_path) { 'gitlab-org/gitlab-ce' }
      let(:condition) do
        {
          source: 'project',
          condition: 'member_of',
          source_id: project_path
        }
      end

      it 'generate the correct url' do
        expect(subject.member_url).to eq("https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-ce/members?per_page=100")
      end
    end
  end
end
