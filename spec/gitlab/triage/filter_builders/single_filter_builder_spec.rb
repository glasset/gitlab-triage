require 'spec_helper'

require 'gitlab/triage/filter_builders/single_filter_builder'

describe Gitlab::Triage::FilterBuilders::SingleFilterBuilder do
  let(:single_filters) do
    {
      'state' => 'closed',
      'milestone' => 'v10.0'
    }
  end

  context '#build_filter' do
    it 'should build the correct filter string' do
      single_filters.each do |k, v|
        builder = described_class.new(k, v)
        expect(builder.build_filter).to eq("&#{k}=#{v}")
      end
    end
  end
end
